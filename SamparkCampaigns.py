import base64
import json
import logging
import os
import time

import pandas as pd
import requests
import waybackpy
from bs4 import BeautifulSoup
from lxml import html
from waybackpy import WaybackMachineAvailabilityAPI, WaybackMachineSaveAPI


def wayback_archival(df, url_col):
    """
    Wayback archival of URL columns in dataframe
    """
    archived_urls = []
    archived_dates = []

    for url in df[url_col]:
        user_agent = "Python Archiver"
        cdx_api = waybackpy.WaybackMachineCDXServerAPI(url, user_agent)
        save_url = False
        try:
            oldest_snapshot = cdx_api.oldest()

            if oldest_snapshot:
                archived_urls.append(oldest_snapshot.archive_url)
                archived_dates.append(pd.to_datetime(
                    oldest_snapshot.timestamp, format='%Y%m%d%H%M%S'))
            else:
                save_url = True
        except waybackpy.exceptions.NoCDXRecordFound:
            save_url = True
        if save_url:
            try:
                save_api = waybackpy.WaybackMachineSaveAPI()
                response = save_api.save(url)

                if response.status_code == 200:
                    archive_url = f"https://web.archive.org/web/{url}"
                    archived_urls.append(archive_url)
                    archived_dates.append(pd.Timestamp.utcnow().floor('s'))
                else:
                    archived_urls.append(None)
                    archived_dates.append(None)
            except:
                archived_urls.append(None)
                archived_dates.append(None)

    df['Archived URL'] = archived_urls
    df['Archived Date'] = archived_dates

    return df


def write_json(new_data, filename):
    with open(filename, 'w') as file:
        json.dump(new_data, file, indent=4)


def get_campaigns():
    url = 'https://sampark.gov.in/Sampark/Campaign_date.jsp'
    s = requests.session()
    response = s.post(
        url=url, data={"fromdate": "01-01-2013", "todate": "30-04-2023"})
    soup = BeautifulSoup(response.text, 'lxml')
    frames = soup.findAll('iframe')
    campaign_urls = []
    for frame in frames:
        campaign_urls.append(frame['src'])
    write_json(campaign_urls, 'data/campaign_URLs.json')
    return campaign_urls


def archive_mailers():
    df = pd.read_json('data/campaign_URLs.json')
    df.columns = ['campaign_url']
    df = wayback_archival(df, url_col='campaign_url')
    with open('data/campaign_URLs.json', 'w') as f:
        f.write(df.to_json(orient='records', lines=True))


def main():
    logging.basicConfig(filename='SamparkCampaigns' + time.strftime("%Y%m%d-%H%M%S") +
                        '.log', format='%(asctime)s %(message)s', level=logging.INFO)
    get_campaigns()
    archive_mailers()


if __name__ == "__main__":
    main()
